package com.example.traccarclient.repository;

import com.example.traccarclient.entity.VerificationCod;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VerificationRepository extends JpaRepository<VerificationCod,Integer> {
    Optional<VerificationCod> findByUserName(String name);
}
