package com.example.traccarclient.repository;

import com.example.traccarclient.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface ClientRepository extends JpaRepository<Client,Integer> {
    Client findByIME(String ime);
}
