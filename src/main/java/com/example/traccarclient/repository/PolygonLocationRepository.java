package com.example.traccarclient.repository;

import com.example.traccarclient.entity.PolygonLocation;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface PolygonLocationRepository extends JpaRepository<PolygonLocation,Integer> {

}
