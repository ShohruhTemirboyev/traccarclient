package com.example.traccarclient.repository;


import com.example.traccarclient.entity.GeometryLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */

public interface GeometryLocRepository extends JpaRepository<GeometryLocation, Integer> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO geometry_location (polygon_id,geog) VALUES (:polygonId,ST_GeomFromText(:polygon,4269));",nativeQuery = true)
    void addPoligon(Integer polygonId,String polygon);

    //CREATE EXTENSION postgis;

    @Query(value = "SELECT st_contains FROM ST_Contains(ST_GeomFromText(:polygon),ST_GeomFromText(:point));",nativeQuery = true)
    Boolean check(String polygon,String point);
    //'POINT(-73.981898 40.768094)'

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM public.geometry_location WHERE polygon_id IN (:id);",nativeQuery = true)
        void deleteByPolygonId(Integer id);

    @Transactional
    @Modifying
    @Query(value = "insert into geometry_location(geog,polygon_circle_id) values(ST_Buffer(ST_GeomFromText(:point), :radius, 'quad_segs=8'),:polygonId);",nativeQuery = true)
    void addPoligonCircle(Integer polygonId,String point,Double radius);


    @Query(value = "select  ST_PointInsideCircle(ST_Point(:lat,:lon),:latCentr,:lonCentr,:radius);",nativeQuery = true)
    Boolean checkCircle(Double lat,Double lon, Double latCentr, Double lonCentr, Double radius);

    //         GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 26910);
//        Coordinate[] coordinates=new Coordinate[]{new Coordinate(1111111,1111111),
//                new Coordinate(1111145,1111167),new Coordinate(1111178,1111112)};
//        Polygon polygon=geometryFactory.createPolygon(coordinates);

}
