package com.example.traccarclient.repository;

import com.example.traccarclient.entity.ClientIme;
import com.example.traccarclient.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface ClientIMERepository extends JpaRepository<ClientIme,Long> {
boolean existsByClinetIme(String id);
boolean existsById(Long id);
ClientIme findByClinetIme(String clientId);
}
