package com.example.traccarclient.repository;

import com.example.traccarclient.entity.Clienthistory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface HistoryRepository extends JpaRepository<Clienthistory,Long> {
    List<Clienthistory> findByIME(String id, Pageable pageable);
//    select*from polygon_location OFFSET p*r ROWS
//    FETCH NEXT r ROWS ONLY;
}
