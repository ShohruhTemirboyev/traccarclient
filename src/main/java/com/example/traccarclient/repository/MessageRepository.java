package com.example.traccarclient.repository;

import com.example.traccarclient.entity.template.Message;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface MessageRepository  extends JpaRepository<Message,Integer> {

    Message findByCode(Integer code);
}
