package com.example.traccarclient.repository;

import com.example.traccarclient.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByUserName(String userName);
    User findByUserName(String userName);
    Optional<User> findById(Long id);
    List<User> findAllByActive(boolean active);

    Page<User> findAllByActive(Boolean check, Pageable pageable);

}
