package com.example.traccarclient.repository;

import com.example.traccarclient.entity.enam.Role;
import com.example.traccarclient.entity.enam.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByRoleName(RoleName roleName);
    Optional<Role> findById(Long roleId);
}
