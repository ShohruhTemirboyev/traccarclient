package com.example.traccarclient.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Clienthistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String IME;
    private Double timestamp;
    private Double lat;
    private Double lon;
    private Double speed;
    private Double bearing;
    private Double altitude;
    private Double accuracy;
    private Double batt;
}
