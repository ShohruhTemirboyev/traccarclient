package com.example.traccarclient.entity;

import com.example.traccarclient.entity.template.AbsEntity;
import com.example.traccarclient.entity.enam.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class User extends AbsEntity implements UserDetails {

    @Column(unique = true,nullable = false)
    private String userName;
    @Column(nullable = false)
    private String password;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String email;
    private Integer language;

    @ManyToMany
    private List<PolygonLocation> polygonLocations;

    @OneToMany
    private List<ClientIme> clientImes;

    @ManyToMany
    private List<PolygonCircleLocation> polygonCircleLocations;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="user_role",
        joinColumns = {@JoinColumn(name = "user_id")},
        inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;


    public User(String userName, String password,  List<Role> roles) {
        this.userName = userName;
        this.password = password;
        this.roles = roles;
    }

    boolean accountNonExpired=true;
boolean accountNonLocked=true;
boolean credentialsNonExpired=true;
boolean enabled=true;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
