package com.example.traccarclient.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.locationtech.jts.geom.Geometry;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class GeometryLocation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer polygonId;

    private Integer polygonCircleId;

    @Column(columnDefinition = "Geometry")
    private Geometry geog;



}
