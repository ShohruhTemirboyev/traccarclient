package com.example.traccarclient.payloat;
import com.example.traccarclient.entity.template.Message;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@NoArgsConstructor
public class ApiResponseModel {
   private Status status;
   private Object data;

   public void setStatus(Message message) {
      this.status =new Status(message.getCode(),message.getMessage(),message.getMessage_ru(), message.getMessage_uz());
   }



}
