package com.example.traccarclient.payloat;

import com.example.traccarclient.entity.template.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Respons {

    private Status status;

    public void setStatus(Message status) {
        this.status =new Status(status.getCode(),status.getMessage(),status.getMessage_ru(), status.getMessage_uz());
    }


}
