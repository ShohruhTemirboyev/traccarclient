package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.MappedSuperclass;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@MappedSuperclass
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Status {
   private  Integer code;
   private String message;
   private String message_ru;
   private String message_uz;
}
