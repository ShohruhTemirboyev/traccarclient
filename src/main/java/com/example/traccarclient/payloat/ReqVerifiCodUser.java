package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqVerifiCodUser {

    private Integer verifiCod;
    private String userName;
}
