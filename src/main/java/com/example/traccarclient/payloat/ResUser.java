package com.example.traccarclient.payloat;

import com.example.traccarclient.entity.enam.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResUser {
    private Long userId;
    private String userName;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String email;
    private Integer language;
    private List<Role> role;

}
