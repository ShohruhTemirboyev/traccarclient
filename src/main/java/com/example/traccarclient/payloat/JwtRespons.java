package com.example.traccarclient.payloat;

import lombok.Data;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
public class JwtRespons {
    private Long userId;
    private String token;
    private String tokenType="Bearer";

    public JwtRespons(String token) {
        this.token = token;
    }
}
