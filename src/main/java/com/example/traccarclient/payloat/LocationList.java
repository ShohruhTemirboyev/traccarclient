package com.example.traccarclient.payloat;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
import com.example.traccarclient.payloat.LanLat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocationList {

    private Long userId;
    private List<List<Double>> listLocation;
    private String name;

}
