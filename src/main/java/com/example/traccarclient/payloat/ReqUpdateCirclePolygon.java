package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqUpdateCirclePolygon {

    private Long userId;

    private Integer polygonId;

    private String name;

    private Double lat;

    private Double lon;

    private Double radius;
}
