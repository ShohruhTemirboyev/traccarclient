package com.example.traccarclient.payloat;

import lombok.Data;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */

@Data
public class ReqUser {

    private String userName;
    private String password;
    private String newPassword;
    private String firstName;
    private String lastName;
    private String middleName;
    private String birthDate;
    private String email;


}
