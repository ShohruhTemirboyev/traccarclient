package com.example.traccarclient.payloat;

import lombok.Data;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
public class ReqLogin {
    private String userName;
    private String password;
}
