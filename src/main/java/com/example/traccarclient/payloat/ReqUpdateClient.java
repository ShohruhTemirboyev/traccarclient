package com.example.traccarclient.payloat;

import lombok.Data;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
public class ReqUpdateClient {

    private Long userId;
    private String name;
    private String clientIME;
    private String newClientIME;
}
