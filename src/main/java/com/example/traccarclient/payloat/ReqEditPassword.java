package com.example.traccarclient.payloat;

import lombok.Data;

@Data
public class ReqEditPassword {

private String userName;

private String oldPassword;

private String newPassword;


}
