package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqPolygonCircleLocation {

    private Long userId;

    private String name;

    private Double lat;

    private Double lon;

    private Double radius;

}
