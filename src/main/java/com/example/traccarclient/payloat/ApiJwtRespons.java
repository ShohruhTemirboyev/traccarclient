package com.example.traccarclient.payloat;

import com.example.traccarclient.entity.template.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiJwtRespons  {

    private Status status;
    private Object data;
//    private Long userId;
//    private String token;
//    private String tokenType = "Bearer";


    public void setStatus(Message message) {
        this.status =new Status(message.getCode(),message.getMessage(),message.getMessage_ru(), message.getMessage_uz());
    }

}
