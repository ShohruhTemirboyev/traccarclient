package com.example.traccarclient.payloat;

import lombok.Data;

@Data
public class ReqDeleteCircle {

    private Long userId;
    private Integer circleId;

}
