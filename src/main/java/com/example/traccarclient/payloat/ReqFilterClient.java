package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqFilterClient {
    private Integer id;
    private String name;
    private String clientIME;
}
