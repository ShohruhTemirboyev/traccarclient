package com.example.traccarclient.payloat;

import lombok.Data;

import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */

@Data
public class ResClient {

    private Integer id;
    private String name;
    private String clientIME;
    private String status;
    private Double lat;
    private Double lon;
}
