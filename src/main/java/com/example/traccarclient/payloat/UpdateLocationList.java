package com.example.traccarclient.payloat;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateLocationList {

    private Long userId;
    private Integer polygonId;
    private List<List<Double>> listLocation;
    private String name;

}
