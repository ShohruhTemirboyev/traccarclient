package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResCirclePolygon {

    private Integer id;

    private String name;

    private Double lat;

    private Double lon;

    private Double radius;
}
