package com.example.traccarclient.payloat;

import lombok.Data;

@Data
public class ReqPermission {
    private Long userId;
    private Long roleId;
}
