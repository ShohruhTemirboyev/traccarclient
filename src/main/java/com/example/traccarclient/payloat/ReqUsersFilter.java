package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReqUsersFilter {

    private String userName;
    private String firstName;
    private String lastName;
    private String middleName;
    private String email;
    private Integer language;
    private Long roleId;
    private String clientIme;
    private Date from_date;
    private Date to_date;
}
