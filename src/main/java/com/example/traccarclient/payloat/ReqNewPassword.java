package com.example.traccarclient.payloat;

import lombok.Data;

@Data
public class ReqNewPassword {

    private String userName;
    private String newPassword;
}
