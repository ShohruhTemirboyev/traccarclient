package com.example.traccarclient.payloat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResAllPolygon {
   private List<ResPolygon> resPolygons;
   private List<ResCirclePolygon> resCirclePolygons;
}
