package com.example.traccarclient.payloat;

import lombok.Data;

@Data
public class ReqUserName {
    private String userName;
}
