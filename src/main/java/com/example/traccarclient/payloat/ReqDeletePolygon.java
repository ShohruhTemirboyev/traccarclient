package com.example.traccarclient.payloat;

import lombok.Data;

@Data
public class ReqDeletePolygon {

    private Long userId;
    private Integer polygonId;

}
