package com.example.traccarclient;

import com.example.traccarclient.messageSender.EmailService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class TraccarClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(TraccarClientApplication.class, args);

    }
    }


