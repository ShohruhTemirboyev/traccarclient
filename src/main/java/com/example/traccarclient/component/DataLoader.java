package com.example.traccarclient.component;

import com.example.traccarclient.entity.User;
import com.example.traccarclient.entity.enam.RoleName;
import com.example.traccarclient.repository.RoleRepository;
import com.example.traccarclient.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")){
            userRepository.save(new User(
                    "Shohruh",
                    passwordEncoder.encode("12345"),
                    Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_ADMIN))));


        }
    }
}
