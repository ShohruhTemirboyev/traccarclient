package com.example.traccarclient.controller;

import com.example.traccarclient.payloat.*;
import com.example.traccarclient.service.impl.ClientServiceImpl;
import com.example.traccarclient.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClientServiceImpl clientService;


  @PostMapping("/addClient")
    public HttpEntity<?> addClient(@RequestBody ReqClient reqClient){
    Respons apiResponse=clientService.addClient(reqClient);
    return ResponseEntity.ok(apiResponse);
  }
  @GetMapping("/getClients/{userId}")
    public HttpEntity<?> getClients(@PathVariable Long userId){
    ApiResponseModel apiResponseModel=clientService.getClients(userId);
    return ResponseEntity.ok(apiResponseModel);
  }

  @GetMapping("/historyClient/{clientId}")
    public HttpEntity<?> getHistory(@PathVariable String clientId, @RequestParam(value = "page",defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page){
    ApiResponseModel apiResponseModel=clientService.getHistoryIME(clientId,page-1,10);
    return ResponseEntity.ok(apiResponseModel);
  }
  @PostMapping("/updateClient")
    public HttpEntity<?> updateClient(@RequestBody ReqUpdateClient reqUpdateClient){
    Respons respons=clientService.updateClient(reqUpdateClient);
    return ResponseEntity.ok(respons);
  }
    @GetMapping("/getFilterClient")
    public HttpEntity<?> getUsers(@RequestBody ReqFilterClient reqFilterClient){
        ApiResponseModel apiResponseModel=clientService.getFilterClient(reqFilterClient);
        return ResponseEntity.ok(apiResponseModel);
    }
//    @GetMapping("/getClientList")
//    public HttpEntity<?> getClientsList(){
//      ApiResponseModel apiResponseModel=clientService
//    }
}
