package com.example.traccarclient.controller;

import com.example.traccarclient.entity.User;
import com.example.traccarclient.messageSender.EmailService;
import com.example.traccarclient.payloat.*;
import com.example.traccarclient.repository.UserRepository;
import com.example.traccarclient.security.CurrentUser;
import com.example.traccarclient.service.impl.UserServiceImpl;
import com.example.traccarclient.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserServiceImpl userService;
    @Autowired
    EmailService emailService;

    @PostMapping("/register")
    public HttpEntity<?> addUser(@RequestBody ReqUser reqUser){
        ApiJwtRespons response=userService.saveUser(reqUser);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqLogin reqLogin){
        ApiResponseModel apiResponseModel=userService.loginUser(reqLogin.getUserName(), reqLogin.getPassword());
        return ResponseEntity.ok(apiResponseModel);
    }
    @GetMapping("/userInfo")
    public HttpEntity<?> getUser(@CurrentUser User user){
        return ResponseEntity.ok(userService.getUser(user));
    }

    @GetMapping("/getFilterUsers")
    public HttpEntity<?> getUsers(@RequestBody ReqUsersFilter reqUsersFilter){
        ApiResponseModel apiResponseModel=userService.getFilterUsers(reqUsersFilter);
      return ResponseEntity.ok(apiResponseModel);
    }

    @PostMapping("/editUser")
    public HttpEntity<?> getEdit(@RequestBody ReqUser reqUser,@RequestParam(value = "userId")Long userId){
         Respons response=userService.editUser(reqUser, userId);
        return ResponseEntity.ok(response);

    }
   @PostMapping("/updateLang/{language}")
  public HttpEntity<?> updateLan(@PathVariable Integer language,@CurrentUser User user){
   ApiResponseModel respons=userService.updateLan(language,user);
   return ResponseEntity.ok(respons);
   }

   @PostMapping("/delete/{id}")
    public HttpEntity<?> deleteUser(@PathVariable Long id){
      return ResponseEntity.ok(userService.deleteUser(id));
   }
   @PostMapping("/login/sendMessageMail")
    public HttpEntity<?> sendMessage(@RequestBody ReqUserName userName){
       Respons respons=userService.sendMessageEmali(userName.getUserName());
       return ResponseEntity.ok(respons);
   }
    @PostMapping("/login/verifiCod")
    public HttpEntity<?> sendVerifiCod(@RequestBody ReqVerifiCodUser reqVerifiCodUser){
        Respons respons=userService.sendVerifiCod(reqVerifiCodUser);
        return ResponseEntity.ok(respons);
    }

    @PostMapping("/login/newPassword")
    public HttpEntity<?> getNewPassword(@RequestBody ReqNewPassword reqNewPassword){
        Respons respons=userService.getNewPassword(reqNewPassword);
        return ResponseEntity.ok(respons);
    }


//    @GetMapping("/userList")
//    public HttpEntity<?> getListUser(){
//         ApiResponseModel apiResponseModel=userService.getUserList();
//         return ResponseEntity.ok(apiResponseModel);
//    }

    @GetMapping("/userList")
    public HttpEntity<?> getPageUserList(@RequestParam(required = false,value = "page") Integer page){
        ApiResponseModel apiResponseModel=userService.getUserPage(page,20);
        return ResponseEntity.ok(apiResponseModel);
    }


    @PostMapping("/editPassword")
    public HttpEntity<?> getEditPassword(@RequestBody ReqEditPassword reqEditPassword){
        Respons respons=userService.getEditPassword(reqEditPassword);
        return ResponseEntity.ok(respons);
    }

    @GetMapping("/roleList")
    public HttpEntity<?> getRoleList(){
        ApiResponseModel apiResponseModel=userService.getRole();
        return ResponseEntity.ok(apiResponseModel);
    }
    @PostMapping("/permission")
    public HttpEntity<?> addPermissionRole(@RequestBody ReqPermission reqPermission){
        Respons respons=userService.addRole(reqPermission);
        return ResponseEntity.ok(respons);
    }
}
