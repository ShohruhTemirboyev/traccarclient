package com.example.traccarclient.controller;

import com.example.traccarclient.payloat.*;
import com.example.traccarclient.service.impl.LocationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    LocationServiceImpl locationService;

    @PostMapping("/addLocation")
    public HttpEntity<?> addloc(@RequestBody LocationList list){
        Respons apiResponse=locationService.addLocation(list);
        return ResponseEntity.ok(apiResponse);
    }
    @PostMapping("/updatePolygonLocation")
    public HttpEntity<?> updateLoc(@RequestBody UpdateLocationList list){
        Respons respons=locationService.updateLocation(list);
        return ResponseEntity.ok(respons);
    }
    @PostMapping("/updateCircleLocation")
    public HttpEntity<?> updateCircleLoc(@RequestBody ReqUpdateCirclePolygon list){
        Respons respons=locationService.updateCircleLocation(list);
        return ResponseEntity.ok(respons);
    }
    @PostMapping("/addCircleLocation")
    public HttpEntity<?> addCircLoc(@RequestBody ReqPolygonCircleLocation reqPolygonCircleLocation){
        Respons respons=locationService.addCircleLocation(reqPolygonCircleLocation);
        return ResponseEntity.ok(respons);
    }
    @GetMapping("/getPolygons/{userId}")
    public HttpEntity<?> getPolygon(@PathVariable Long userId){
        ApiResponseModel apiResponseModel=locationService.getPolygons(userId);
        return ResponseEntity.ok(apiResponseModel);
    }

    @GetMapping("/getPolygonsList")
    public HttpEntity<?> getPolygonList(){
        ApiResponseModel apiResponseModel=locationService.getPolygonsList();
        return ResponseEntity.ok(apiResponseModel);
    }

    @PostMapping("/delete/polygon")
    public HttpEntity<?> delete(@RequestBody ReqDeletePolygon reqDeletePolygon){
        Respons respons=locationService.deletePolygon(reqDeletePolygon);
        return ResponseEntity.ok(respons);
    }
    @PostMapping("/delete/circle")
    public HttpEntity<?> deleteCircle(@RequestBody ReqDeleteCircle reqDeletCircle){
        Respons respons=locationService.deleteCircle(reqDeletCircle);
        return ResponseEntity.ok(respons);
    }

}
