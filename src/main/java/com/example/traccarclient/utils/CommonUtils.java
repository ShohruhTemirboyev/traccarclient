package com.example.traccarclient.utils;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class CommonUtils {
    public static void validatePageAndSize(int page,int size){
        if (page<0 || size>AppConstants.MAX_PAGE_SIZE){
            throw new RuntimeException("Page manfiy va size"+AppConstants.MAX_PAGE_SIZE+"bo'lishi mumkin emas");
        }
    }


    public static Pageable getPageable(Integer page,Integer size){
        Pageable pageable= PageRequest.of(page, size, Sort.Direction.DESC,"id");
        validatePageAndSize(page,size);
        return pageable;
    }
}
