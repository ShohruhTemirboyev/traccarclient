package com.example.traccarclient.service;

import com.example.traccarclient.entity.User;
import com.example.traccarclient.payloat.*;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-17
 */
public interface UserService {

    ApiJwtRespons saveUser(ReqUser reqUser);
    ApiResponseModel loginUser(String userName, String password);
    JwtRespons getJwtRespons(String phoneNumber, String password);
    Respons editUser(ReqUser reqUser, Long userId);
    ApiResponseModel updateLan(Integer language, User user);
    Respons deleteUser(Long id);
    ApiResponseModel getUser(User user);
    ApiResponseModel getFilterUsers(ReqUsersFilter reqUsersFilter);
    Respons sendMessageEmali(String userName);
    Respons sendVerifiCod(ReqVerifiCodUser reqVerifiCodUser);
    Respons getNewPassword(ReqNewPassword reqNewPassword);
}
