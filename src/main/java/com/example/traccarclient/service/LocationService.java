package com.example.traccarclient.service;

import com.example.traccarclient.entity.PolygonCircleLocation;
import com.example.traccarclient.entity.PolygonLocation;
import com.example.traccarclient.payloat.*;

import java.util.List;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-17
 */
public interface LocationService {
    Respons addLocation(LocationList list);
    Respons updateLocation(UpdateLocationList list);
    Respons updateCircleLocation(ReqUpdateCirclePolygon list);
    Respons addCircleLocation(ReqPolygonCircleLocation reqPolygonCircleLocation);
    ApiResponseModel getPolygons(Long userId);
    ResCirclePolygon getCirclePolygon(PolygonCircleLocation polygonCircleLocation);
    ResPolygon getPolygon(PolygonLocation polygonLocation);
    List<List<Double>> getLatLon(String polygon);
}
