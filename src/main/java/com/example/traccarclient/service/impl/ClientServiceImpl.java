package com.example.traccarclient.service.impl;

import com.example.traccarclient.entity.*;
import com.example.traccarclient.payloat.*;
import com.example.traccarclient.repository.*;
import com.example.traccarclient.service.ClientService;
import com.example.traccarclient.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientIMERepository clientIMERepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    GeometryLocRepository geometryLocRepository;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    HistoryRepository historyRepository;

    @Autowired
    MessageRepository messageRepository;

    private final JdbcTemplate jdbcTemp;

    public ClientServiceImpl(JdbcTemplate jdbcTemp) {
        this.jdbcTemp = jdbcTemp;
    }

    public Respons addClient(ReqClient reqClient){
        Respons respons=new Respons();
        try {
            if (!clientIMERepository.existsByClinetIme(reqClient.getClientIME())){
                ClientIme client=new ClientIme();
                Optional<User> user=userRepository.findById(reqClient.getUserId());
                client.setName(reqClient.getName());
                client.setClinetIme(reqClient.getClientIME());
                clientIMERepository.save(client);
                user.get().getClientImes().add(client);
                userRepository.save(user.get());
                respons.setStatus(messageRepository.findByCode(0));
            }
            else {
                respons.setStatus(messageRepository.findByCode(1007));
            }
        }
        catch (Exception exception){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }



    public ApiResponseModel getClients(Long userId){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            Optional<User> userOptional=userRepository.findById(userId);
            if (userOptional.isPresent()){
            List<ResClient> resClientList=userOptional.get().getClientImes().stream().map(clientIme -> getClient(clientIme,userOptional.get().getPolygonLocations(),userOptional.get().getPolygonCircleLocations())).collect(Collectors.toList());
            resClientList.remove(null);
            if (resClientList.size()==0){
                apiResponseModel.setStatus(messageRepository.findByCode(1005));
            }
            else {
                apiResponseModel.setStatus(messageRepository.findByCode(0));
                apiResponseModel.setData(resClientList);
            }
            }else {
                apiResponseModel.setStatus(messageRepository.findByCode(101));
            }

        }catch (Exception exception){
           apiResponseModel.setStatus(messageRepository.findByCode(1));
            System.out.println(exception);
        }
        return apiResponseModel;
    }


    public ResClient getClient(ClientIme clientIdentifikator,List<PolygonLocation> polygonLocationList,List<PolygonCircleLocation> polygonCircleLocationList){
        ResClient resClient=new ResClient();
        Optional<Client> client= Optional.ofNullable(clientRepository.findByIME(clientIdentifikator.getClinetIme()));
        if (client.isPresent()) {
            String point = "POINT(" + client.get().getLat() + " " + client.get().getLon() + ")";
            Boolean check = false;
            resClient.setStatus(null);
            for (PolygonLocation poly : polygonLocationList) {
                check = geometryLocRepository.check(poly.getPolygon(), point);
                if (check) {
                    resClient.setStatus(poly.getName());

                }
            }
            check=false;
            for (PolygonCircleLocation poly : polygonCircleLocationList) {
                check = geometryLocRepository.checkCircle(
                        client.get().getLat(),
                        client.get().getLon(),
                        poly.getLat(),
                        poly.getLon(),
                        poly.getRadius());
                if (check) {
                    resClient.setStatus("circle:"+poly.getName());

                }
            }
            resClient.setLat(client.get().getLat());
            resClient.setLon(client.get().getLon());
            resClient.setId(clientIdentifikator.getId());
            resClient.setName(clientIdentifikator.getName());
            resClient.setClientIME(clientIdentifikator.getClinetIme());
            return resClient;
        }
        return null;
    }


    public ApiResponseModel getHistoryIME(String ime,int page ,int size) {
        ApiResponseModel apiResponseModel = new ApiResponseModel();
        try {
            List<Clienthistory> clienthistories = historyRepository.findByIME(ime, CommonUtils.getPageable(page,size));
            apiResponseModel.setStatus(messageRepository.findByCode(0));
            apiResponseModel.setData(clienthistories);

        } catch (Exception e) {
            apiResponseModel.setStatus(messageRepository.findByCode(1));

        }
        return apiResponseModel;
    }


    public Respons updateClient(ReqUpdateClient reqUpdateClient) {
    Respons respons=new Respons();
    try {
        Optional<User> user=userRepository.findById(reqUpdateClient.getUserId());
        if (user.isPresent()){
        if (!clientIMERepository.existsByClinetIme(reqUpdateClient.getNewClientIME())){
             Optional<ClientIme> clientImeOptional= Optional.ofNullable(clientIMERepository.findByClinetIme(reqUpdateClient.getClientIME()));
             if (clientImeOptional.isPresent()){
                 clientImeOptional.get().setId(clientImeOptional.get().getId());
                 clientImeOptional.get().setClinetIme(reqUpdateClient.getNewClientIME());
                 clientImeOptional.get().setName(reqUpdateClient.getName());
             clientIMERepository.save(clientImeOptional.get());
             respons.setStatus(messageRepository.findByCode(0));
             }
             else {
                 respons.setStatus(messageRepository.findByCode(1005));
             }
        }
        else {
            respons.setStatus(messageRepository.findByCode(1007));
          }
        }
        else {
            respons.setStatus(messageRepository.findByCode(101));
        }
    }
    catch (Exception e){
        respons.setStatus(messageRepository.findByCode(1));
    }
    return respons;
    }


    public ApiResponseModel getFilterClient(ReqFilterClient reqFilterClient) {
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            String str="";
            if (reqFilterClient.getId()!=null){
                str+=" and id="+reqFilterClient.getId()+" ";
            }
            if (reqFilterClient.getName()!=null){
                str += " and upper(c.name) like upper('%" +reqFilterClient.getName() + "%')";
            }
            if (reqFilterClient.getClientIME()!=null){
                str += " and upper(c.clinet_ime) like upper('%" +reqFilterClient.getClientIME() + "%')";
            }
            if (str==null)
            {
                List<Map<String,Object>> maps1=jdbcTemp.queryForList("select c.id,c.clinet_ime, c.name  from client_ime c");
                apiResponseModel.setStatus(messageRepository.findByCode(0));
                apiResponseModel.setData(maps1);
            }
            if (str!=null){
                List<Map<String,Object>> maps2=jdbcTemp.queryForList("select c.id,c.clinet_ime, c.name  from client_ime c where id>0 "+str);
                apiResponseModel.setStatus(messageRepository.findByCode(0));
                apiResponseModel.setData(maps2);
            }
        }catch (Exception e){
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }


    public ApiResponseModel getClientList(){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {


        }catch (Exception e){
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }


}
