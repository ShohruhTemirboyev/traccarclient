package com.example.traccarclient.service.impl;

import com.example.traccarclient.entity.User;
import com.example.traccarclient.entity.VerificationCod;
import com.example.traccarclient.entity.enam.Role;
import com.example.traccarclient.entity.enam.RoleName;
import com.example.traccarclient.messageSender.EmailService;
import com.example.traccarclient.payloat.*;
import com.example.traccarclient.repository.MessageRepository;
import com.example.traccarclient.repository.RoleRepository;
import com.example.traccarclient.repository.UserRepository;
import com.example.traccarclient.repository.VerificationRepository;
import com.example.traccarclient.security.JwtTokenProvider;
import com.example.traccarclient.service.UserService;
import com.example.traccarclient.utils.CommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import static com.example.traccarclient.entity.enam.RoleName.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Service
public class UserServiceImpl implements UserDetailsService , UserService {


    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    EmailService emailService;

    @Autowired
    VerificationRepository verificationRepository;

    private final Integer min=123456;
    private final Integer max=987654;

    private final JdbcTemplate jdbcTemplate;

    public UserServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }



    public ApiJwtRespons saveUser(ReqUser reqUser){
        ApiJwtRespons response=new ApiJwtRespons();
        try {
            if (!userRepository.existsByUserName(reqUser.getUserName())){
                User user=new User();
                user.setUserName(reqUser.getUserName());
                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                user.setFirstName(reqUser.getFirstName());
                user.setLastName(reqUser.getLastName());
                user.setMiddleName(reqUser.getMiddleName());
                user.setEmail(reqUser.getEmail());
                user.setActive(true);
                user.setBirthDate(reqUser.getBirthDate());
                user.setRoles((Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_USER))));
                userRepository.save(user);
                response.setStatus(messageRepository.findByCode(0));
                JwtRespons jwtRespons=getJwtRespons(reqUser.getUserName(),reqUser.getPassword());
                jwtRespons.setUserId(user.getId());
                response.setData(jwtRespons);

        }
            else {
                response.setStatus(messageRepository.findByCode(105));
            }
        }
        catch (Exception ex){
         response.setStatus(messageRepository.findByCode(1));
        }
        return response;
    }
    public ApiResponseModel loginUser(String userName,String password){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            Optional<User> userOptional= Optional.ofNullable(userRepository.findByUserName(userName));
            if (userOptional.isPresent()) {
                if (passwordEncoder.matches(password,userOptional.get().getPassword())){
                JwtRespons jwtRespons = getJwtRespons(userName, password);
                jwtRespons.setUserId(userOptional.get().getId());
                apiResponseModel.setData(jwtRespons);
                apiResponseModel.setStatus(messageRepository.findByCode(0));
                }
                else {
                    apiResponseModel.setStatus(messageRepository.findByCode(106));
                }
            }
            else {
                apiResponseModel.setStatus(messageRepository.findByCode(101));
            }
        }
        catch (Exception e){
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }

    public JwtRespons getJwtRespons(String phoneNumber,String password){
        Authentication authentication=authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(phoneNumber,password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new JwtRespons(jwtTokenProvider.generateToken(authentication));
    }

    public Respons editUser(ReqUser reqUser, Long userId){
        Respons response=new Respons();
        try {
            Optional<User>userOptional=userRepository.findById(userId);
            if (userOptional.isPresent()){
                        userOptional.get().setId(userId);
                        userOptional.get().setUserName(reqUser.getUserName());
                        userOptional.get().setFirstName(reqUser.getFirstName());
                        userOptional.get().setLastName(reqUser.getLastName());
                        userOptional.get().setMiddleName(reqUser.getMiddleName());
                        userOptional.get().setEmail(reqUser.getEmail());
                        userOptional.get().setBirthDate(reqUser.getBirthDate());
                        userRepository.save(userOptional.get());
                        response.setStatus(messageRepository.findByCode(0));
            }
            else {
                response.setStatus(messageRepository.findByCode(101));
            }

        }
        catch (Exception ex){
          response.setStatus(messageRepository.findByCode(1));
        }
        return response;
    }
    public ApiResponseModel updateLan(Integer language,User user){
        ApiResponseModel respons=new ApiResponseModel();
        try {
              user.setLanguage(language);
              userRepository.save(user);
               respons.setData(user);
               respons.setStatus(messageRepository.findByCode(0));
        }
        catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }
    public Respons deleteUser(Long id){
        Respons respons=new Respons();
        try {
        Optional<User> userOptional=userRepository.findById(id);
        if (userOptional.isPresent()){
            userOptional.get().setActive(false);
            userRepository.save(userOptional.get());
            respons.setStatus(messageRepository.findByCode(0));
        }
        else {
            respons.setStatus(messageRepository.findByCode(101));
        }
        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }


    public ApiResponseModel getUser(User user){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            apiResponseModel.setStatus(messageRepository.findByCode(0));
            apiResponseModel.setData( new ResUser(
                    user.getId(),
                    user.getUsername(),
                    user.getFirstName(),
                    user.getLastName(),
                    user.getLastName(),
                    user.getBirthDate(),
                    user.getEmail(),
                    user.getLanguage(),
                    user.getRoles()));
               }catch (Exception e){
                   apiResponseModel.setStatus(messageRepository.findByCode(1));
               }
        return apiResponseModel;

    }
    public ApiResponseModel getFilterUsers(ReqUsersFilter reqUsersFilter){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            String str="";
            if (reqUsersFilter.getUserName()!=null){
                str += " and upper(u.user_name) like upper('%" + reqUsersFilter.getUserName() + "%')";
            }
            if (reqUsersFilter.getFirstName()!=null){
                str += " and upper(u.first_name) like upper('%" + reqUsersFilter.getFirstName() + "%')";
            }
            if (reqUsersFilter.getLastName()!=null){
                str += " and upper(u.last_name) like upper('%" + reqUsersFilter.getLastName() + "%')";
            }
            if (reqUsersFilter.getMiddleName()!=null){
                str += " and upper(u.middle_name) like upper('%" + reqUsersFilter.getMiddleName() + "%')";
            }
            if (reqUsersFilter.getEmail()!=null){
                str += " and upper(u.email) like upper('%" + reqUsersFilter.getEmail()+ "%')";
            }
            if (reqUsersFilter.getLanguage()!=null){
                str += " and CAST(u.language as TEXT) like '%" + reqUsersFilter.getLanguage()+ "%'";
            }
            if (reqUsersFilter.getFrom_date()!=null && reqUsersFilter.getTo_date()!=null){
                str += " and (u.created_at between '" + reqUsersFilter.getFrom_date() + "' and '" + reqUsersFilter.getTo_date() + "')";
            }
            if (str.length()==0 && reqUsersFilter.getRoleId()==null && reqUsersFilter.getClientIme()==null){
                List<Map<String,Object>> maps=jdbcTemplate.queryForList("select u.id,u.user_name,u.first_name,u.last_name,u.middle_name,u.email,u.language,u.created_at from users u where u.active=true");
                for (int i = 0; i <maps.size() ; i++) {
                    Long id=(Long)maps.get(i).get("id");
                    List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.id, r.role_name from user_role ur inner join role r on ur.role_id = r.id where ur.user_id =?",id);
                    List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select c.id,c.clinet_ime, c.name from users_client_imes uci inner join client_ime c on uci.client_imes_id = c.id where uci.users_id = ?",id);
                    maps.get(i).put("roles",maps1);
                    maps.get(i).put("client_ime",maps2);
                }
                apiResponseModel.setData(maps);
                apiResponseModel.setStatus(messageRepository.findByCode(0));
            }
            else {
                if (reqUsersFilter.getRoleId() != null && reqUsersFilter.getClientIme() != null) {
                    List<Map<String, Object>> mapsA = jdbcTemplate.queryForList("select u.id,u.user_name,u.first_name,u.last_name,u.middle_name,u.email,u.language,u.created_at from users u inner join user_role ur on u.id=ur.user_id inner join users_client_imes uci on u.id = uci.users_id inner join client_ime ci on uci.client_imes_id = ci.id where ur.role_id='"+reqUsersFilter.getRoleId()+"' and ci.clinet_ime='"+reqUsersFilter.getClientIme()+"' and u.active=true" + str);
                        for (int i = 0; i < mapsA.size(); i++) {
                            Long id = (Long) mapsA.get(i).get("id");
                            List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.id, r.role_name from user_role ur inner join role r on ur.role_id = r.id where ur.user_id =?", id);
                            List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select c.id,c.clinet_ime, c.name from users_client_imes uci inner join client_ime c on uci.client_imes_id = c.id where uci.users_id =?", id);
                            mapsA.get(i).put("roles", maps1);
                            mapsA.get(i).put("client_ime", maps2);
                        }
                        apiResponseModel.setStatus(messageRepository.findByCode(0));
                        apiResponseModel.setData(mapsA);
                        return apiResponseModel;
                }
                else {
                    List<Map<String, Object>> mapsB = jdbcTemplate.queryForList("select u.id,u.user_name,u.first_name,u.last_name,u.middle_name,u.email,u.language,u.created_at from users u inner join user_role ur on u.id=ur.user_id where ur.role_id=" + reqUsersFilter.getRoleId() + " and u.active=true" + str);
                    if (reqUsersFilter.getRoleId() != null) {
                        for (int i = 0; i < mapsB.size(); i++) {
                            Long id = (Long) mapsB.get(i).get("id");
                            List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.id, r.role_name from user_role ur inner join role r on ur.role_id = r.id where ur.user_id =?", id);
                            List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select c.id,c.clinet_ime, c.name from users_client_imes uci inner join client_ime c on uci.client_imes_id = c.id where uci.users_id =?", id);
                            mapsB.get(i).put("roles", maps1);
                            mapsB.get(i).put("client_ime", maps2);
                        }
                        apiResponseModel.setStatus(messageRepository.findByCode(0));
                        apiResponseModel.setData(mapsB);
                    }
                    if (reqUsersFilter.getClientIme() != null) {
                        List<Map<String, Object>> mapsC = jdbcTemplate.queryForList("select u.id,u.user_name,u.first_name,u.last_name,u.middle_name,u.email,u.language,u.created_at from users u inner join users_client_imes uci on u.id = uci.users_id inner join client_ime ci on uci.client_imes_id = ci.id where  ci.clinet_ime='"+reqUsersFilter.getClientIme()+"' and u.active=true" + str);
                        for (int i = 0; i < mapsC.size(); i++) {
                            Long id = (Long) mapsC.get(i).get("id");
                            List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select c.id,c.clinet_ime, c.name from users_client_imes uci inner join client_ime c on uci.client_imes_id = c.id where uci.users_id = ?", id);
                            List<Map<String, Object>> maps1 = jdbcTemplate.queryForList("select r.id, r.role_name from user_role ur inner join role r on ur.role_id = r.id where ur.user_id =?", id);
                            mapsC.get(i).put("client_ime", maps2);
                            mapsC.get(i).put("roles", maps1);
                        }
                        apiResponseModel.setStatus(messageRepository.findByCode(0));
                        apiResponseModel.setData(mapsC);
                        return apiResponseModel;
                    }

                }
            }
                if (str != null && reqUsersFilter.getClientIme() == null && reqUsersFilter.getRoleId() == null) {
                    List<Map<String, Object>> maps9 = jdbcTemplate.queryForList("select u.id,u.user_name,u.first_name,u.last_name,u.middle_name,u.email,u.language,u.created_at  from users u where u.active=true" + str);
                    for (int i = 0; i < maps9.size(); i++) {
                        Long id = (Long) maps9.get(i).get("id");
                        List<Map<String, Object>> maps2 = jdbcTemplate.queryForList("select r.id, r.role_name from user_role ur inner join role r on ur.role_id = r.id where ur.user_id =?", id);
                        List<Map<String, Object>> maps3 = jdbcTemplate.queryForList("select c.id,c.clinet_ime, c.name from users_client_imes uci inner join client_ime c on uci.client_imes_id = c.id where uci.users_id = ?", id);
                        maps9.get(i).put("roles", maps2);
                        maps9.get(i).put("client_ime", maps3);
                    }
                    apiResponseModel.setStatus(messageRepository.findByCode(0));
                    apiResponseModel.setData(maps9);
                }


        }
        catch (Exception e){
            System.out.println(e);
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }

    @Override
    public Respons sendMessageEmali(String userName) {
        Respons respons=new Respons();
        try {
            Optional<User> userOptional= Optional.ofNullable(userRepository.findByUserName(userName));
            if (userOptional.isPresent()) {
                if (userOptional.get().getEmail()!=null) {

                    Integer verifiCod= ThreadLocalRandom.current().nextInt(min, max + 1);
                    Optional<VerificationCod> verificationCodOptional=verificationRepository.findByUserName(userName);

                    if (verificationCodOptional.isPresent()) {
                        verificationCodOptional.get().setVerifiCod(verifiCod);
                        verificationCodOptional.get().setCount(0);
                        verificationRepository.save(verificationCodOptional.get());
                        emailService.sendMail(userOptional.get().getEmail(), "Human control Verification Cod", "Никому не сообщайте код.Код доступа:"+verifiCod);
                        respons.setStatus(messageRepository.findByCode(109));

                    }
                    else {
                        VerificationCod verificationCod=new VerificationCod();
                    verificationCod.setUserName(userName);
                    verificationCod.setVerifiCod(verifiCod);
                    verificationCod.setCount(0);
                    verificationRepository.save(verificationCod);
                    emailService.sendMail(userOptional.get().getEmail(), "Human control Verification Cod", "Никому не сообщайте код.Код доступа:"+verifiCod);
                    respons.setStatus(messageRepository.findByCode(109));
                    }
                  }

                else {
                    respons.setStatus(messageRepository.findByCode(108));
                }
                }
            else {
                respons.setStatus(messageRepository.findByCode(101));
            }

        }
        catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }

    @Override
    public Respons sendVerifiCod(ReqVerifiCodUser reqVerifiCodUser) {
        Respons respons=new Respons();
        try {
            Optional<VerificationCod> verificationCod=verificationRepository.findByUserName(reqVerifiCodUser.getUserName());
            if (verificationCod.isPresent()) {
                if (verificationCod.get().getCount() < 3) {
                    if (verificationCod.get().getVerifiCod().equals(reqVerifiCodUser.getVerifiCod())) {
                        verificationRepository.delete(verificationCod.get());
                        respons.setStatus(messageRepository.findByCode(0));
                    } else {
                        verificationCod.get().setCount(verificationCod.get().getCount() + 1);
                        verificationRepository.save(verificationCod.get());
                        respons.setStatus(messageRepository.findByCode(1002));
                    }
                } else {
                    verificationRepository.delete(verificationCod.get());
                    respons.setStatus(messageRepository.findByCode(1001));
                }
            }else {
                respons.setStatus(messageRepository.findByCode(1003));
            }
        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }
    public Respons getNewPassword(ReqNewPassword reqNewPassword){
        Respons respons=new Respons();
        try {
            Optional<User> userOptional= Optional.ofNullable(userRepository.findByUserName(reqNewPassword.getUserName()));
            if (userOptional.isPresent()){
                userOptional.get().setPassword(passwordEncoder.encode(reqNewPassword.getNewPassword()));
                userRepository.save(userOptional.get());
                respons.setStatus(messageRepository.findByCode(0));
            }
            else {
                respons.setStatus(messageRepository.findByCode(101));
            }

        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }
    public ApiResponseModel getUserList(){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            List<ResUser> resUsers = userRepository.findAll().stream().map(user -> getUserinfo(user)).collect(Collectors.toList());
            apiResponseModel.setStatus(messageRepository.findByCode(0));
            apiResponseModel.setData(resUsers);
        }catch (Exception e){
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }

    public ResUser getUserinfo(User user){
        ResUser resUser=new ResUser(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getLastName(),
                user.getBirthDate(),
                user.getEmail(),
                user.getLanguage(),
                user.getRoles());
        return resUser;
    }


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return userRepository.findByUserName(userName);
    }
    public UserDetails loadUserByUserId(Long id) {
         return  userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));

    }


    public ApiResponseModel getUserPage(Integer page,Integer size) {
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            if (page!=null){
            List<ResUser> resUsers=userRepository.findAllByActive(true, CommonUtils.getPageable(page-1,size)).stream().map(user -> getUserinfo(user)).collect(Collectors.toList());
            apiResponseModel.setStatus(messageRepository.findByCode(0));
            apiResponseModel.setData(resUsers);}
            else {
                List<ResUser> resUsers = userRepository.findAll().stream().map(user -> getUserinfo(user)).collect(Collectors.toList());
                apiResponseModel.setStatus(messageRepository.findByCode(0));
                apiResponseModel.setData(resUsers);
            }

        }catch (Exception e){
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }

    public Respons getEditPassword(ReqEditPassword reqEditPassword){
        Respons respons=new Respons();
        try {
            Optional<User> userOptional= Optional.ofNullable(userRepository.findByUserName(reqEditPassword.getUserName()));
            if (userOptional.isPresent()){
                if (passwordEncoder.matches(reqEditPassword.getOldPassword(),userOptional.get().getPassword())){
                     userOptional.get().setPassword(passwordEncoder.encode(reqEditPassword.getNewPassword()));
                     userRepository.save(userOptional.get());
                     respons.setStatus(messageRepository.findByCode(0));
                 }else {
                     respons.setStatus(messageRepository.findByCode(106));
                 }
            }else {
                respons.setStatus(messageRepository.findByCode(101));
            }


        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }

    public ApiResponseModel getRole(){
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            List<Map<String,Object>> maps=jdbcTemplate.queryForList("select * from role");
            apiResponseModel.setStatus(messageRepository.findByCode(0));
            apiResponseModel.setData(maps);

        }catch (Exception e){
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }

    public Respons addRole(ReqPermission reqPermission){
        Respons respons=new Respons();
        try {
            Optional<User> userOptional=userRepository.findById(reqPermission.getUserId());
            if (userOptional.isPresent()){
                Optional<Role> role=roleRepository.findById(reqPermission.getRoleId());
                if (role.isPresent()){
                userOptional.get().getRoles().add(role.get());
                userRepository.save(userOptional.get());
                respons.setStatus(messageRepository.findByCode(0));
                }
                else {
                    respons.setStatus(messageRepository.findByCode(1004));
                }
            }else {
                respons.setStatus(messageRepository.findByCode(101));
            }

        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }
}
