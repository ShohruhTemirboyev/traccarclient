package com.example.traccarclient.service.impl;

import com.example.traccarclient.entity.PolygonCircleLocation;
import com.example.traccarclient.entity.PolygonLocation;
import com.example.traccarclient.entity.User;
import com.example.traccarclient.payloat.*;
import com.example.traccarclient.repository.*;
import com.example.traccarclient.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-12
 */
@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    GeometryLocRepository geometryLocRepository;

    @Autowired
    ClientIMERepository clientIMERepository;

    @Autowired
    PolygonLocationRepository polygonLocationRepository;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PolyCircleRepository polyCircleRepository;

    /**
     * @param list
     * @return
     */
    public Respons addLocation(LocationList list){
        Respons apiResponse=new Respons();
        try {

            Optional<User> userOptional=userRepository.findById(list.getUserId());
            if (userOptional.isPresent()){
                String str = "POLYGON((";
                for (List<Double> latlon: list.getListLocation()) {
                    str += latlon.get(0) + " " + latlon.get(1) + ",";
                }
                str = str.substring(0, str.length() - 1);
                str += "))";
                PolygonLocation polygonLocation=new PolygonLocation();
                polygonLocation.setPolygon(str);
                polygonLocation.setName(list.getName());
                polygonLocationRepository.save(polygonLocation);
                userOptional.get().getPolygonLocations().add(polygonLocation);
                userRepository.save(userOptional.get());
                geometryLocRepository.addPoligon(polygonLocation.getId(),str);
                apiResponse.setStatus(messageRepository.findByCode(0));
           }
            else {
                  apiResponse.setStatus(messageRepository.findByCode(101));
            }
        }
        catch (Exception exception){
            apiResponse.setStatus(messageRepository.findByCode(1));
        }
        return apiResponse;
    }


    public Respons updateCircleLocation(ReqUpdateCirclePolygon list) {
        Respons respons=new Respons();
        try {
            Optional<User> userOptional=userRepository.findById(list.getUserId());
            if (userOptional.isPresent()){
                String str="POINT("+list.getLat()+" "+list.getLon()+")";
                Optional<PolygonCircleLocation> polygonCircleLocationOptional=polyCircleRepository.findById(list.getPolygonId());
                if (polygonCircleLocationOptional.isPresent()){
                    polygonCircleLocationOptional.get().setId(list.getPolygonId());
                    polygonCircleLocationOptional.get().setName(list.getName());
                    polygonCircleLocationOptional.get().setLat(list.getLat());
                    polygonCircleLocationOptional.get().setLon(list.getLon());
                    polygonCircleLocationOptional.get().setRadius(list.getRadius()/100000);
                polyCircleRepository.save(polygonCircleLocationOptional.get());
                geometryLocRepository.addPoligonCircle(list.getPolygonId(),str, list.getRadius()/100000);
                respons.setStatus(messageRepository.findByCode(0));
                }
                else {
                    respons.setStatus(messageRepository.findByCode(1008));
                }
            }
            else {
                respons.setStatus(messageRepository.findByCode(101));
            }

        }
        catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }

    public Respons updateLocation(UpdateLocationList list){
        Respons apiResponse=new Respons();
        try {
            Optional<User> userOptional=userRepository.findById(list.getUserId());
            if (userOptional.isPresent()){
                String str = "POLYGON((";
                for (List<Double> latlon : list.getListLocation()) {
                    str += latlon.get(0) + " " + latlon.get(1) + ",";
                }
                str = str.substring(0, str.length() - 1);
                str += "))";
                     Optional<PolygonLocation> polygonLocationOptional=polygonLocationRepository.findById(list.getPolygonId());
                    if (polygonLocationOptional.isPresent()){
                        polygonLocationOptional.get().setId(list.getPolygonId());
                        polygonLocationOptional.get().setName(list.getName());
                        polygonLocationOptional.get().setPolygon(str);
                        polygonLocationRepository.save(polygonLocationOptional.get());
                        geometryLocRepository.deleteByPolygonId(list.getPolygonId());
                        geometryLocRepository.addPoligon(list.getPolygonId(),str);
                        apiResponse.setStatus(messageRepository.findByCode(0));
                    }
                    else {
                        apiResponse.setStatus(messageRepository.findByCode(1008));
                    }
                }
            else {
                apiResponse.setStatus(messageRepository.findByCode(101));
            }
        }
        catch (Exception exception){
            apiResponse.setStatus(messageRepository.findByCode(1));
        }
        return apiResponse;
    }


    public Respons addCircleLocation(ReqPolygonCircleLocation reqPolygonCircleLocation){
        Respons respons=new Respons();
        try {
            Optional<User> userOptional=userRepository.findById(reqPolygonCircleLocation.getUserId());
            if (userOptional.isPresent()){
                String str="POINT("+reqPolygonCircleLocation.getLat()+" "+reqPolygonCircleLocation.getLon()+")";
                PolygonCircleLocation polygonCircleLocation=new PolygonCircleLocation();
                polygonCircleLocation.setName(reqPolygonCircleLocation.getName());
                polygonCircleLocation.setLat(reqPolygonCircleLocation.getLat());
                polygonCircleLocation.setLon(reqPolygonCircleLocation.getLon());
                polygonCircleLocation.setRadius(reqPolygonCircleLocation.getRadius()/100000);
                polyCircleRepository.save(polygonCircleLocation);
                userOptional.get().getPolygonCircleLocations().add(polygonCircleLocation);
                userRepository.save(userOptional.get());
                geometryLocRepository.addPoligonCircle(polygonCircleLocation.getId(),str, reqPolygonCircleLocation.getRadius()/100000);
                respons.setStatus(messageRepository.findByCode(0));
            }
            else {
                respons.setStatus(messageRepository.findByCode(101));
            }
        }
        catch (Exception exception){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }

    @Override
    public ApiResponseModel getPolygons(Long userId) {
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {
            Optional<User> userOptional=userRepository.findById(userId);
            if (userOptional.isPresent()){
                if (userOptional.get().getPolygonLocations().size()!=0 || userOptional.get().getPolygonCircleLocations().size()!=0) {
                    List<ResPolygon> resPolygons =userOptional.get().getPolygonLocations().stream().map(polygonLocation -> getPolygon(polygonLocation)).collect(Collectors.toList());
                    List<ResCirclePolygon> resCirclePolygons=userOptional.get().getPolygonCircleLocations().stream().map(polygonCircleLocation ->getCirclePolygon(polygonCircleLocation)).collect(Collectors.toList());
                    apiResponseModel.setStatus(messageRepository.findByCode(0));
                    ResAllPolygon resAllPolygon=new ResAllPolygon(resPolygons,resCirclePolygons);
                    apiResponseModel.setData(resAllPolygon);
                }
                else {
                    apiResponseModel.setStatus(messageRepository.findByCode(107));
                }
            }
            else {
                apiResponseModel.setStatus(messageRepository.findByCode(101));
            }
        }
        catch (Exception e){
            System.out.println(e);
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }
    public ApiResponseModel getPolygonsList() {
        ApiResponseModel apiResponseModel=new ApiResponseModel();
        try {

                    List<ResPolygon> resPolygons =polygonLocationRepository.findAll().stream().map(polygonLocation -> getPolygon(polygonLocation)).collect(Collectors.toList());
                    List<ResCirclePolygon> resCirclePolygons=polyCircleRepository.findAll().stream().map(polygonCircleLocation ->getCirclePolygon(polygonCircleLocation)).collect(Collectors.toList());
                    apiResponseModel.setStatus(messageRepository.findByCode(0));
                    ResAllPolygon resAllPolygon=new ResAllPolygon(resPolygons,resCirclePolygons);
                    apiResponseModel.setData(resAllPolygon);
        }
        catch (Exception e){
            System.out.println(e);
            apiResponseModel.setStatus(messageRepository.findByCode(1));
        }
        return apiResponseModel;
    }


    public ResCirclePolygon getCirclePolygon(PolygonCircleLocation polygonCircleLocation){
           ResCirclePolygon resCirclePolygon=new ResCirclePolygon();
           resCirclePolygon.setId(polygonCircleLocation.getId());
           resCirclePolygon.setName(polygonCircleLocation.getName());
           resCirclePolygon.setLat(polygonCircleLocation.getLat());
           resCirclePolygon.setLon(polygonCircleLocation.getLon());
           resCirclePolygon.setRadius(polygonCircleLocation.getRadius()*100000);
           return resCirclePolygon;
    }


    public ResPolygon getPolygon(PolygonLocation polygonLocation){
        ResPolygon resPolygon=new ResPolygon();
         resPolygon.setId(polygonLocation.getId());
         resPolygon.setName(polygonLocation.getName());
         resPolygon.setLatlon(getLatLon(polygonLocation.getPolygon()));
        return resPolygon;
    }


    public List<List<Double>> getLatLon(String polygon){
        List<List<Double>> latlon=new ArrayList<List<Double>>();
        polygon=polygon.substring(9,polygon.length()-2);
        String[] param=new String[2];
        int i=0;
        Double lat=null;
        for (String s:polygon.split(",")){
            List<Double> list = new ArrayList<Double>();
            param=s.split(" ");
            lat=Double.parseDouble(param[0]);
            list.add(lat);
            lat=Double.parseDouble(param[1]);
            list.add(lat);
            latlon.add(list);
        }
        return latlon;
    }


    public Respons deletePolygon(ReqDeletePolygon reqDeletePolygon){
        Respons respons=new Respons();
        try {
            Optional<User> userOptional=userRepository.findById(reqDeletePolygon.getUserId());
            if (userOptional.isPresent()){
                Optional<PolygonLocation> polygonLocation=polygonLocationRepository.findById(reqDeletePolygon.getPolygonId());
                if (polygonLocation.isPresent()){
                    userOptional.get().getPolygonLocations().remove(polygonLocation.get());
                    userRepository.save(userOptional.get());
                    polygonLocationRepository.delete(polygonLocation.get());
                    respons.setStatus(messageRepository.findByCode(0));
                }else {
                    respons.setStatus(messageRepository.findByCode(107));
                }
            }else {
                respons.setStatus(messageRepository.findByCode(101));
            }

        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }

    public Respons deleteCircle(ReqDeleteCircle reqDeletCircle) {
        Respons respons=new Respons();
        try {
            Optional<User> userOptional=userRepository.findById(reqDeletCircle.getUserId());
            if (userOptional.isPresent()){
                Optional<PolygonCircleLocation> polygonLocation=polyCircleRepository.findById(reqDeletCircle.getCircleId());
                if (polygonLocation.isPresent()){
                    userOptional.get().getPolygonCircleLocations().remove(polygonLocation.get());
                    userRepository.save(userOptional.get());
                    polyCircleRepository.delete(polygonLocation.get());
                    respons.setStatus(messageRepository.findByCode(0));
                }else {
                    respons.setStatus(messageRepository.findByCode(107));
                }
            }else {
                respons.setStatus(messageRepository.findByCode(101));
            }

        }catch (Exception e){
            respons.setStatus(messageRepository.findByCode(1));
        }
        return respons;
    }
}
