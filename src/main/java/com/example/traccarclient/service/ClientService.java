package com.example.traccarclient.service;

import com.example.traccarclient.entity.ClientIme;
import com.example.traccarclient.entity.PolygonCircleLocation;
import com.example.traccarclient.entity.PolygonLocation;
import com.example.traccarclient.payloat.*;

import java.util.List;
/**
 *  @author Shohruh Temirboyev
 *  @since   2022-02-17
 */
public interface ClientService {
    Respons addClient(ReqClient reqClient);
    ApiResponseModel getClients(Long userId);
    ResClient getClient(ClientIme clientIdentifikator, List<PolygonLocation> polygonLocationList, List<PolygonCircleLocation> polygonCircleLocationList);
    ApiResponseModel getHistoryIME(String ime,int page,int size);
    Respons updateClient(ReqUpdateClient reqUpdateClient);
    ApiResponseModel getFilterClient(ReqFilterClient reqFilterClient);
}
